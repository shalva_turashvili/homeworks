fun main(){
    val arr = arrayOf(1,2,3,4,5);
    println(first(arr));
    val test: String = "Don't nod."
    println(second(test))
}

fun first(arr: Array<Int>): Int{
    var result = 0;
    for (i in 0..arr.count()) {
        if (i % 2 == 0)
            result += arr[i];
    }
    return result;
}

fun second(text: String): Boolean{
    val toFilter = arrayOf('.', ',', ' ', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '\'', '"','?')
    val cleanText = text.filter { !toFilter.contains(it) }.lowercase()

    if (cleanText == cleanText.reversed())
        return true;

    return false;
}