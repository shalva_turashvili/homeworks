import java.lang.Math.pow
import kotlin.math.sqrt

class Point(x: Double, y: Double) {
    var X: Double = x
    var Y: Double = y

    fun ToString(): String {
        return "X: $X Y: $Y"
    }

    fun Equals(pointOne: Point): Boolean {
        if (pointOne.X == X && pointOne.Y == Y)
            return true
        return false
    }

    fun Equals(pointOne:Point, pointTwo:Point):Boolean{
        if (pointOne.X == pointTwo.X && pointOne.Y == pointTwo.Y)
            return true
        return false
    }

    fun SymetricMove() {
        X *= -1
        Y *= -1
    }

    fun GetLength(point: Point):Double {
        return sqrt(pow(point.X - X, 2.0) + pow(point.Y - Y, 2.0))
    }
}