abstract class AbstractFactory<T>{
    abstract fun create(type: String): T?;
}