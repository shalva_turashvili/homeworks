fun main(){
    val factory : AbstractFactory<Car> = CarFactory();
    val car = factory.create("Audi")
    car?.drive();
}